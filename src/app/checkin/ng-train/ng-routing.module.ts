import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { Router, RouterModule, Routes } from "@angular/router";
import { NgIfComponentComponent } from "./ng-if-component/ng-if-component.component";
import { NgModuleComponent } from "./ng-module/ng-module.component";
import { NgSwitchComponent } from "./ng-switch/ng-switch.component";
import { NgTrainComponent } from "./ng-train.component";
import { NgforComponent } from "./ngfor/ngfor.component";

const routes : Routes = [
    { 
        path: 'NgTrainComponent' , 
        component: NgTrainComponent,
        children: [
            { path: 'ngIfComponent' , component: NgIfComponentComponent },
            { path: 'NgforComponent' , component: NgforComponent },
            { path: 'NgModuleComponent' , component: NgModuleComponent},
            { path: 'ngSwitch' , component: NgSwitchComponent}
        ]
    },
    { path: '' , redirectTo: 'NgTrainComponent' , pathMatch: 'full' }

]

@NgModule({
    imports: [CommonModule,RouterModule.forChild(routes)],
    exports: [RouterModule] 
})


export class ngTrain{

}