import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ngfor',
  templateUrl: './ngfor.component.html',
  styleUrls: ['./ngfor.component.scss']
})
export class NgforComponent implements OnInit {

  Teatre : string = 'satyam';

  constructor() { }
  

  items: Item[] = [
    {movie:'Zootopia',time:'Sat Jun 15 2019 ',tcktprice:21},
    {movie:'Batman v Superman: Dawn of Justice',time:'Sat Jun 15 2019 22:21:08 GMT+0530',tcktprice:25},
    {movie:'Captain America: Civil War',time:'Sat Jun 15 2019 ',tcktprice:30},
    {movie:'X-Men: Apocalypse',time:'2019-06-15T16:51:08.681Z ',tcktprice:50}
  ]
 
  ngOnInit(): void {
  }



}

export interface Item{
  movie:string;
  time:string;
  tcktprice:number;
}
