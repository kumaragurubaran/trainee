import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-if-component',
  templateUrl: './ng-if-component.component.html',
  styleUrls: ['./ng-if-component.component.scss']
})
export class NgIfComponentComponent implements OnInit {
  clicked: boolean= false;
  logIn: boolean = false;

  name = 'Me';

  usAvail = true;
  constructor() { }

  ngOnInit(): void {
  }

  Action(){
    this.logIn = !this.logIn;
    this.usAvail = false;
  }

}
