import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Routes } from '@angular/router';

@Component({
  selector: 'app-ng-train',
  templateUrl: './ng-train.component.html',
  styleUrls: ['./ng-train.component.scss']
})
export class NgTrainComponent implements OnInit {
  clicked: boolean= true;
  ngIf:boolean = false;
  ngFor:boolean = false;
  ngModule:boolean = false;
  ngSwitch:boolean =false;
  constructor(private route :Router ,private routes : ActivatedRoute) { }

  ngOnInit(): void { 
  }

  changeAction(val: string){
    this.clicked = false;
    if(val == 'ngIf'){
      this.ngIf = true;
      this.ngFor = false;
      this.ngModule = false;
      this.clicked = !this.clicked;
      this.route.navigate(["ngIfComponent"], {relativeTo:this.routes})
    }
    if(val == 'ngFor'){
      this.ngIf = false;
      this.ngModule = false;
      this.ngFor = true;
      this.clicked = !this.clicked;
      this.route.navigate(["NgforComponent"], {relativeTo:this.routes})
    }
    if(val == 'ngModule'){
      this.ngIf = false;
      this.ngFor = false;
      this.ngModule = true;
      this.clicked = !this.clicked;
      this.route.navigate(["NgModuleComponent"], {relativeTo:this.routes})
    }
    if(val == 'ngSwitch'){
      this.ngIf = false;
      this.ngFor = false;
      this.ngModule = false;
      this.ngSwitch = true;
      this.clicked = !this.clicked;
      this.route.navigate(["ngSwitch"], {relativeTo:this.routes})
    }
  }

}
