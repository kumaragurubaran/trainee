import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgTrainComponent } from './ng-train.component';

describe('NgTrainComponent', () => {
  let component: NgTrainComponent;
  let fixture: ComponentFixture<NgTrainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NgTrainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NgTrainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
