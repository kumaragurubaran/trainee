import { NgModule } from '@angular/core';
import { NgIfComponentComponent } from './ng-if-component/ng-if-component.component';
import { NgforComponent } from './ngfor/ngfor.component';
import { NgModuleComponent } from './ng-module/ng-module.component';
import { ngTrain } from './ng-routing.module';
import { CommonModule } from '@angular/common';
import { NgSwitchComponent } from './ng-switch/ng-switch.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [NgIfComponentComponent,NgforComponent,NgModuleComponent, NgSwitchComponent],
  imports: [
    ngTrain,
    CommonModule,FormsModule
  ]
})
export class NgTrainModule { }
