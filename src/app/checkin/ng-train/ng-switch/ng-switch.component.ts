import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-switch',
  templateUrl: './ng-switch.component.html',
  styleUrls: ['./ng-switch.component.scss']
})
export class NgSwitchComponent implements OnInit {

  constructor() { }

  selectedColor:string ='black';

  items: Code[] =[
  {colour : 'black', value: 1} , 
  {colour : 'yellow', value: 4} , 
  {colour : 'green', value: 7}  ,
  {colour : 'red', value: 11}  
  ]
  ngOnInit(): void {
  }

}

export interface Code{
  colour:string;
  value:number;
}
