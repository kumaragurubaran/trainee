import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-homepg',
  templateUrl: './homepg.component.html',
  styleUrls: ['./homepg.component.scss']
})
export class HomepgComponent implements OnInit {
  clicked: boolean=false;
  routeBtn: boolean = false;
  noRoute: boolean = true;

  constructor(private routes :Router, private route : ActivatedRoute) { }

  ngOnInit(): void {
  }

  changeCss(){
    this.clicked = !this.clicked;
    this.noRoute = false;
    this.routeBtn = true;
  }

  navigate(){
    this.routes.navigate(["..","NgTrainComponent"], { relativeTo: this.route} );
  }

  day = new Date();
  bucks = 50.15;
}
