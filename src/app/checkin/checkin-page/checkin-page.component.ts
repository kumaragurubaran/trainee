import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-checkin-page',
  templateUrl: './checkin-page.component.html',
  styleUrls: ['./checkin-page.component.scss']
})
export class CheckinPageComponent implements OnInit {

  constructor(private routes:Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
  }

  navigate(){
    this.routes.navigate(["homepg"], { relativeTo: this.route });
  }

}
