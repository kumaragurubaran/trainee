import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CheckinPageComponent } from './checkin-page/checkin-page.component';
import { HomepgComponent } from './homepg/homepg.component';
import {MatButtonModule} from '@angular/material/button';
import { RouterModule, Routes } from '@angular/router';
import { NgTrainComponent } from './ng-train/ng-train.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import { Checkin } from './checkin-routing.module';
import { CheckinComponent } from './checkin.component';
import { NgTrainModule } from './ng-train/ng-train.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    CheckinPageComponent,
    HomepgComponent,
    NgTrainComponent,
    CheckinComponent,
   
  ],
  imports: [
    CommonModule,MatButtonModule,RouterModule,MatSidenavModule,Checkin,NgTrainModule,FormsModule
  ],
  exports:[
    CheckinComponent
  ]
})
export class CheckinModule { }
