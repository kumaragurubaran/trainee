import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CheckinPageComponent } from "./checkin-page/checkin-page.component";
import { HomepgComponent } from "./homepg/homepg.component";
import { NgTrainComponent } from "./ng-train/ng-train.component";

const routes: Routes= [
    { path: 'homepg' , component: HomepgComponent},
    { path: '', component: CheckinPageComponent, pathMatch: 'full' },
    { path: 'NgTrainComponent', loadChildren: () => import('./ng-train/ng-train.module').then((m)=>m.NgTrainModule)}
    
]
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class Checkin{
}