import { NgModule } from "@angular/core";
import { Grandchild2Component } from "./grandchild2.component";
import { GreatgrandchildComponent } from "./greatgrandchild/greatgrandchild.component";
import { GreatGrandChildrenModule } from "./greatgrandchild/greatGrandChildModule";

@NgModule({
    declarations:[Grandchild2Component],
    imports:[GreatGrandChildrenModule],
    exports:[]
})

export class GrandChildModule{}