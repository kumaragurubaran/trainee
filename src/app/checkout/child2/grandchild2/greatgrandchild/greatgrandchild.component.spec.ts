import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GreatgrandchildComponent } from './greatgrandchild.component';

describe('GreatgrandchildComponent', () => {
  let component: GreatgrandchildComponent;
  let fixture: ComponentFixture<GreatgrandchildComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GreatgrandchildComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GreatgrandchildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
