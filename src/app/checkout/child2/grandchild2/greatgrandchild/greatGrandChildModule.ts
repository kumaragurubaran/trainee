import { NgModule } from "@angular/core";
import { GreatgrandchildComponent } from "./greatgrandchild.component";

@NgModule({
    declarations:[GreatgrandchildComponent],
    imports:[],
    exports:[GreatgrandchildComponent],
})

export class GreatGrandChildrenModule{}