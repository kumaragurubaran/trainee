import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GrandChild1Component } from './grand-child1/grand-child1.component';
import { Grandchild2Component } from './grandchild2/grandchild2.component';

const routes: Routes = [
    {path:'grandchild1',component:GrandChild1Component},
    {path:'grandchild2',component:Grandchild2Component},
    // {path:'',redirectTo:'grandchild2',pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GrandsModuleRoutingModule { }