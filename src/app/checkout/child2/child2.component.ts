import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-child2',
  templateUrl: './child2.component.html',
  styleUrls: ['./child2.component.scss']
})
export class Child2Component implements OnInit {

  constructor(private route : Router, private navigate:ActivatedRoute) { }

  ngOnInit(): void {
  }

  navi(){
    this.route.navigate(['grandchild2'],{relativeTo:this.navigate})
  }

}
