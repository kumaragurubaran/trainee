import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GrandsModuleRoutingModule } from './grands-module-routing';
import { GrandChildModule } from './grandchild2/grandchildModule';

@NgModule({
  declarations: [
    
  ],
  imports: [
    CommonModule,
    GrandsModuleRoutingModule,GrandChildModule
  ]
})
export class GrandsModuleModule { }