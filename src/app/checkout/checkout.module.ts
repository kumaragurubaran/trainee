import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CheckoutRoutingModule } from './checkout-routing.module';
import { Child1Component } from './child1/child1.component';
import { Child2Component } from './child2/child2.component';
import { Child3Component } from './child3/child3.component';
import { GrandChild1Component } from './child2/grand-child1/grand-child1.component';
import { CheckoutComponent } from './checkout.component';


@NgModule({
  declarations: [
    Child1Component,
    Child2Component,
    Child3Component,
    GrandChild1Component,
    CheckoutComponent
  ],
  imports: [
    CommonModule,
    CheckoutRoutingModule
  ]
})
export class CheckoutModule { }
