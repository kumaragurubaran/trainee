import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {

  constructor(private routes: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
  }

  navigate(val:string){
    if(val=='child1'){
      this.routes.navigate(['Child1Component'], {relativeTo:this.route})
    }
    if(val=='child2'){
      this.routes.navigate(['Child2Component'],{relativeTo:this.route})
    }
    if(val=='child3'){
      this.routes.navigate(['Child3Component'],{relativeTo:this.route})
    }
  }

}
