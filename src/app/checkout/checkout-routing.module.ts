import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CheckoutComponent } from './checkout.component';
import { CheckoutModule } from './checkout.module';
import { Child1Component } from './child1/child1.component';
import { Child2Component } from './child2/child2.component';
import { Child3Component } from './child3/child3.component';

const routes: Routes = [
  {
    path:'CheckoutComponent',component:CheckoutComponent,
    children:[ 
    { path:'Child1Component',component:Child1Component },
    { path:'Child2Component',
      component:Child2Component,
      loadChildren: () => import('./child2/grands-module.module').then((m)=>m.GrandsModuleModule)},
    { path:'Child3Component',component:Child3Component },
  ]
  },
  {path:'',redirectTo:'CheckoutComponent',pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CheckoutRoutingModule { }
