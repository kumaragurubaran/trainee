import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';

const routes: Routes = [
  {
    path: 'home',
    component: AppComponent
  },
  {
    path: 'checkin',
    loadChildren: () => import('./checkin/checkin.module').then((m) => m.CheckinModule)
  },
  {
    path: '',
    redirectTo: 'checkout',
    pathMatch: 'full'
  },
  {
    path:'checkout',
    loadChildren: () => import('./checkout/checkout.module').then((m) => m.CheckoutModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
