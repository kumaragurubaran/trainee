import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'conCurr'
})
export class ConCurrPipe implements PipeTransform {

  transform(value: number, ...args: number[]): unknown {
    const [bucks] = args;
    return value*bucks;
  }

}
