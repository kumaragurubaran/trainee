import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CheckinModule } from './checkin/checkin.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CheckoutComponent } from './checkout/checkout.component';
import { ConCurrPipe } from './pipes/con-curr.pipe';
import { CheckinComponent } from './checkin/checkin.component';
import { CheckoutModule } from './checkout/checkout.module';
import { JunkComponent } from './junk/junk.component';


@NgModule({
  declarations: [
    AppComponent,
    ConCurrPipe,
    JunkComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, BrowserAnimationsModule,CheckinModule,CheckoutModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
